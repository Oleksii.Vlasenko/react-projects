import React, {Component} from 'react';
import './CreateTask.css';

export default class CreateTask extends Component {

    constructor(props) {
        super(props);

        this.state = {task: ''};

        this.onChangeInput = this.onChangeInput.bind(this);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    /**
     * @desc Form validator
     **/
    onClickHandler() {
        const {onSubmit} = this.props;

        if (typeof onSubmit === 'function') {
            onSubmit({
                id: (Math.random() * 100).toFixed(),
                title: this.state.task
            });
        }

        this.setState({task: ''});
    }

    onChangeInput(e) {
        this.setState({task: e.target.value});
    }

    /**
     * @desc
     **/
    render() {
        return (
            <div className="create-task">
                <div>
                    <label htmlFor="taskEdit">Задача</label>
                    <input type="text"
                           id="taskEdit"
                           onChange={this.onChangeInput}
                           value={this.state.task}
                           name="task"
                           className="form-element"/>
                </div>

                <div>
                    <button
                        type="button"
                        onClick={this.onClickHandler}> Добавить
                    </button>
                </div>


            </div>
        );
    }
}