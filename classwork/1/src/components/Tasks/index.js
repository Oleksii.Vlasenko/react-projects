import React, {Component} from 'react';
import CreateTask from './Create';
import List from './List';

export default class Tasks extends Component {
    /**
     *
     **/
    constructor(props) {
        super(props);

        this.state = {list: []};

        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onElementDelete = this.onElementDelete.bind(this);
    }

    /**
     * @desc Form submit
     **/
    onFormSubmit(task) {
        // const prevState = this.state.list;
        // prevState.push(task);
        // this.setState({list: prevState});

        console.log('Task -->', task);

        this.setState(state => {
            return {
                    list: [
                        ...state.list,
                        ...[task]
                    ]
                }
            }
        );
    }


    onElementDelete(item) {
        if (item) {
            const prevState = this.state.list;

            const index = prevState
                .findIndex(i => i.id === item.id);

            if (index >= 0) {
                prevState.splice(index, 1);

                this.setState({list: prevState});
            }
        }
    }

    /**
     * @desc Render
     **/
    render() {
        return (
            <div className="tasks">
                <CreateTask onSubmit={task => this.onFormSubmit(task)}/>

                <List items={this.state.list}
                      onDeleteItem={this.onElementDelete}/>
            </div>
        );
    }
}