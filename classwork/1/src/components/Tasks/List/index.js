import React, {Component} from 'react';
import ListItem from './ListItem';

export default class List extends Component {

    onDeleteItem(item) {
        const {onDeleteItem} = this.props;

        if (typeof onDeleteItem === 'function') {
            onDeleteItem(item);
        }
    }

    /**
     * @desc Render component
     **/
    render() {
        // const {items} = this.props;
        const items = this.props.items;

        return (
            <div className="create-task">
                <ul>
                    {
                        items.map(item =>
                            <ListItem key={item.id}
                                      item={item}
                                      onDeleteItem={this.onDeleteItem.bind(this)}
                                    />
                        )
                    }
                </ul>
            </div>
        );
    }
}