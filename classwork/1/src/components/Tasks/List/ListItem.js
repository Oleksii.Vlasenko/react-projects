import React, {Component} from 'react';

export default class ListItem extends Component {
    /**
     *
     **/
    onClickHandler() {
        const {onDeleteItem, item} = this.props;

        if (typeof onDeleteItem === 'function') {
            onDeleteItem(item);
        }
    }

    /**
     **/
    render() {
        const {item} = this.props;

        return (
            <li className="list-item">
                <strong>{item.title}</strong>
                <button type="button" onClick={this.onClickHandler.bind(this)}>Удалить</button>
            </li>
        );
    }
}