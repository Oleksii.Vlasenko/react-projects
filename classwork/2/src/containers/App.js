import React, {Component} from 'react';
import Store from "../services/Store";
import styled from 'styled-components';
import createStore from "../services/CreateStore";

const Table = styled.div`
  display: inline-block;
  font-size: 30px;
`;

const Btn = styled.button`
    width: 50px;
    height: 50px;
    border-radius: 5px;
    background-color: #f3e2d1;
`;

const initialState = {count: 0};

function reducer(state, action) {
    switch (action.type) {
        case 'INCREMENT':
            return {count: state.count + action.amount};
        case 'DECREMENT':
            return {count: state.count - action.amount};
        case 'RESET':
            return {count: 0};
        default:
            return state;
    }
}

const incrementAction = {type: 'INCREMENT', amount: 1};
const decrementAction = {type: 'DECREMENT', amount: 1};
const resetAction = {type: 'RESET'};

const store = createStore(reducer, initialState);

export default class App extends Component {

    constructor(props) {
        super(props);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.reset = this.reset.bind(this);
        this.unSubscribe1 = store.subscribe(() => console.log(store.getState()));
    }

    componentDidMount() {
        store.subscribe(() => this.forceUpdate())
        this.unSubscribe2 = store.subscribe(() => console.log(store.getState()));
    }

    increment() {
        store.dispatch(incrementAction);
        console.log(this.unSubscribe2);
        if (store.getState().count > 2) {
            this.unSubscribe2();
        }
    }

    decrement() {
        store.dispatch(decrementAction);
        console.log(this.unSubscribe1);
        if (store.getState().count < -2) {
            this.unSubscribe1();
        }
    }

    reset() {
      store.dispatch(resetAction);
    }

    render() {
      const count = store.getState().count;
        return (
            <div>
                <Table>{count}</Table>
                <Btn onClick={this.increment}>INC</Btn>
                <Btn onClick={this.reset}>RES</Btn>
                <Btn onClick={this.decrement}>DEC</Btn>
            </div>
        );
    }
}

