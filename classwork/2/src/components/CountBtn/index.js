import React, {Component} from "react";
import styled from 'styled-components';

const Btn = styled.button`
    width: 50px;
    height: 50px;
    border-radius: 5px;
    background-color: #f3e2d1;
`;


export default class CountBtn extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <Btn onClick={this.props.action}>{this.props.text}</Btn>
        )
    }
}
