import React, {useState} from 'react';
import {createStore} from "redux";

export default function Section(props) {
    const inc = {type: 'INCREMENT'};
    const dec = {type: 'DECREMENT'};
    const res = {type: 'RESET'};

    const reducer = (state = 0, action) => {
        switch (action.type) {
            case 'INCREMENT':
                return state + 1;
            case 'DECREMENT':
                return state - 1;
            case 'RESET':
                return 0;
            default:
                return state;
        }
    };

    const store = createStore(reducer);
    const unSubscribe = store.subscribe(() => console.log(store.getState()));

    const increment = () => {
        store.dispatch(inc);
    };

    return (
        <div>
            <p>Count: {}</p>
            <button onClick={() => increment()}>INC</button>
            {/*<button onClick={decrement}>DEC</button>*/}
            {/*<button onClick={reset}>RES</button>*/}
        </div>
    )
}
