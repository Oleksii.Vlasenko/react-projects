import React, {useState} from "react";
import styled from 'styled-components';
import Card from "../CardList/Card/Card";
import {useDispatch, useSelector} from "react-redux";
import CartForm from './CartForm/index';
import {removeFromCart} from "../../../store/actions";
import submit from "./CartForm/submit";

const CartBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    position: relative;
`;

const BuyBtn = styled.button`
    width: 100px;
    height: 60px;
    background-color: #dd3333;
    color: #ffffff;
    font-size: 24px;
    font-weight: 700;
    position: fixed;
    right: 0px;
    top: 120px;
    border: none;
    border-radius: 10px;
`;

const Cart = ({action}) => {

    let state = useSelector(state => state.cart);
    let dispatch = useDispatch();
    let [cartForm, setCartForm] = useState(false);

    const cardModal = {
        header: 'Remove from cart',
        closeButton: true,
        text: `Do you want to remove this album?`,
        actionBtn: [`Remove`, `Cancel`]
    };

    const openForm = () => {
        setCartForm(!cartForm);
    };

    const calcTotalPrice = () => {
        let sum = state.reduce((sum, item) => (+sum + +item.price), 0);
        let result = Math.trunc(sum * 100) / 100;
        return result;
    }

    const handleSubmit = (values) => {

        console.log('VALUES ===> ', values);
        console.log('YOU SHOULD TO PAY ---> ', calcTotalPrice(), '$');

        state.map((item) => dispatch(removeFromCart(item)));
        setCartForm(!cartForm);
    };

    const cancelForm = () => {
        setCartForm(!cartForm);
    };

    return (
        <CartBlock>
            {state && (state.length > 0) && state.map((item, index) => {
                return (
                    <Card key={index}
                          album={item}
                          btnRemTitle={'Remove'}
                          cardModal={cardModal}
                          action={action}/>
                )
            })}
            {!cartForm && <BuyBtn onClick={openForm}>BUY</BuyBtn>}
            {cartForm && <CartForm cancel={cancelForm}
                                   active={handleSubmit}
                                   onSubmit={handleSubmit}/>}
        </CartBlock>
    )
};

export default Cart;
