import React from "react";
import {Field, reduxForm} from "redux-form";
import styled from 'styled-components';
import submit from "./submit";

const FormBlock = styled.div`
    width: 100vw;
    height: 100vh;
    background-color: #00000088;
    position: fixed;
    top: 0;
    left: 0;
    Z-index: 9999; 
`;

const Form = styled.form`
    width: 400px;
    height: 300px;
    margin: 200px auto;
    background-color: #7777ff;
    border-radius: 20px;
`;

const FormLine = styled.div`
    text-align: center;
    padding-top: 20px;
`;

const FormButton = styled.button`
    width: 75px;
    height: 30px;
    border: none;
    border-radius: 15px;
    margin: 0 20px;
`;

let CartForm = ({cancel, handleSubmit, active}) => {

    const submitValidate = (values) => submit(values)
        .then(result => {
            if(result) active(values)
        });

    return (
        <FormBlock>
            <Form onSubmit={handleSubmit(submitValidate)}>
                <FormLine>
                    <Field name="firstname" component="input" type="text" placeholder={`firstname`}/>
                </FormLine>
                <FormLine>
                    <Field name="lastname" component="input" type="text" placeholder={`lastname`}/>
                </FormLine>
                <FormLine>
                    <Field name="age" component="input" type="number" placeholder={'age'}/>
                </FormLine>
                <FormLine>
                    <Field name="address" component="input" type="text" placeholder={'address'}/>
                </FormLine>
                <FormLine>
                    <Field name="tel" component="input" type="tel" placeholder={'tel'}/>
                </FormLine>
                <FormLine>
                    <FormButton type="button" onClick={cancel}>Cancel</FormButton>
                    <FormButton type="submit">Checkout</FormButton>
                </FormLine>
            </Form>
        </FormBlock>
    )
};

CartForm = reduxForm({
    form: 'user'
})(CartForm);

export default CartForm;
