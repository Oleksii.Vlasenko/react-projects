import React, {useEffect, useState} from "react";
import styled from 'styled-components'
import CardList from "./CardList/CardList";
import Cart from "./Cart";
import Favorite from "./Favorite";
import {Switch, Route} from "react-router-dom";
import Modal from "./Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {addToCard, modalWindow, removeFromCart} from "../../store/actions";
import getAlbums from "../../store/actions/albumsAction";

const SectionContainer = styled.div`
    background-image: url(/img/bg_section.jpg);
    background-size: cover;
`;

const ConfirmBtn = styled.button`
    width: 101px;
    height: 41px;
    border-radius: 3px;
    background-color: #b3382c;
    color: #ffffff;
    font-size: 15px;
    border: none;
    margin: 0 5px;
`;

const Section = () => {
    let [activeAlbum, setActiveAlbum] = useState({});
    let [cardModal, setCardModal] = useState({});

    let modal = useSelector(state => state.modal);
    let dispatch = useDispatch();

    useEffect(() => {
        getAlbums()(dispatch)
    }, [dispatch]);

    const initCartModal = (cardModal, album) => {
        dispatch(modalWindow());
        setActiveAlbum(album);
        setCardModal(cardModal);
    };

    const closeModal = (event) => {
        dispatch(modalWindow());
        if (event === `Add`) {
            dispatch(addToCard(activeAlbum));
        }
        if (event === `Remove`) {
            dispatch(removeFromCart(activeAlbum));
        }
    };

    const createActions = (array) => {
        let key = 0;
        let result = array.map((item) => {
            return (<ConfirmBtn onClick={() => closeModal(item)}
                                key={key++}>{item}</ConfirmBtn>)
        });
        return result;
    };

     return (
         <SectionContainer>
             <Switch>
                 <Route exact path='/' render={() => <CardList action={initCartModal}/>}/>
                 <Route path='/cart' render={() => <Cart action={initCartModal}/>}/>
                 <Route path='/favorite' render={() => <Favorite action={initCartModal}/>}/>
             </Switch>
             {modal && <Modal action={closeModal}
                                    header={cardModal.header}
                                    closeButton={cardModal.closeButton}
                                    text={cardModal.text}
                                    actions={createActions(cardModal.actionBtn)}/>}
         </SectionContainer>
    )
};

export default Section;
