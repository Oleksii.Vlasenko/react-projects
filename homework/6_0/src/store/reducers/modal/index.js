import C from "../../consts";

const modal = (state = false, action) => {
    switch(action.type) {
        case C.MODAL:
            return !state;
        default:
            return state;
    }
};

export default modal;
