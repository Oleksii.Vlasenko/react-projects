import C from "../../consts";

const initialState = [];

const addToCart = (state, action) => {
    return [
        ...state,
        action.album
    ]
};

const removeFromCart = (state, action) => {
    return state.filter((item) => item.article !== action.album.article)
};

const getLocalRequest = () => {
    const state = JSON.parse(localStorage.getItem(`cart`)) || [];
    return state;
};

const cart = (state = initialState, action) => {
    switch (action.type) {
        case C.ADD_TO_CART:
            return addToCart(state, action);
        case C.REM_FROM_CART:
            return removeFromCart(state, action);
        case C.GET_REQUEST:
            return getLocalRequest();
        default:
            return state;

    }
};

export default cart;
