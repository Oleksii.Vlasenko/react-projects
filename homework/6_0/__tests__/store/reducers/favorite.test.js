import C from "../../../src/store/consts/index";
import favorite from "../../../src/store/reducers/favorite";
import deepFreeze from 'deep-freeze'

describe("get request", () => {
    it("ADD_TO_FAVORITE success", () => {
        const state = [];
        const action = {
            type: C.ADD_TO_FAVORITE,
            album: {
                test: 'test'
            }
        };
        deepFreeze(state);
        deepFreeze(action);
        const results = favorite(state, action);
        expect(results)
            .toEqual([{test: 'test'}]);
    });
    it("REM_FROM_FAVORITE success", () => {
        const state = [{article: "0001"}, {article: "0002"}];
        const action = {
            type: C.REM_FROM_FAVORITE,
            album: {
                article: "0001"
            }
        };
        const results = favorite(state, action);
        expect(results)
            .toEqual([{article: "0002"}]);
    })
});
