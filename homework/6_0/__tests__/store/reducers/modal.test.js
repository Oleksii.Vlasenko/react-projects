import C from "../../../src/store/consts/index";
import modal from "../../../src/store/reducers/modal";
import deepFreeze from 'deep-freeze'

describe("get request", () => {
    it("MODAL success", () => {
        const state = true;
        const action = {
            type: C.MODAL
        };
        deepFreeze(state);
        deepFreeze(action);
        const results = modal(state, action);
        expect(results)
            .toEqual(false);
    });
});
