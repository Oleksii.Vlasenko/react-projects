import React, {Component} from "react";
import styled from 'styled-components';
import PropTypes from 'prop-types';

const OpenBtn = styled.button`
color: #ff0000;
width: 270px;
height: 60px;
font-size: 20px;
font-width: 900;
text-transform: uppercase;
`;

export default class OpenModalBtn extends Component {
    render() {
        let {text, onClick, backgroundColor} = this.props;
        return (
            <OpenBtn style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</OpenBtn>
        )
    }
}

OpenModalBtn.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func
};
