import React, {Component} from 'react';
import './App.css';
import OpenModalBtn from "../components/OpenModalBtn";
import Modal from "../components/Modal";
import styled from "styled-components";

const ConfirmBtn = styled.button`
    width: 101px;
    height: 41px;
    border-radius: 3px;
    background-color: #b3382c;
    color: #ffffff;
    font-size: 15px;
    border: none;
    margin: 0 5px;
`;

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            first: false,
            second: false
        };
    }

    openFirstModal() {
        this.setState({first: true});
    }

    closeModal() {
        this.setState({first: false});
        this.setState({second: false});
    }

    openSecondModal() {
        this.setState({second: true});
    }

    createActions(array) {
        let key = 0;
        let result = array.map((item) => <ConfirmBtn onClick={this.closeModal.bind(this)}
                                                     key={key++}>{item}</ConfirmBtn>);
        return result;
    }

    render() {
        const firstModal = {
            header: 'Do you want to delete this file?',
            closeButton: true,
            text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
            actions: this.createActions([`Ok`, `Cancel`])
        };
        const secondModal = {
            header: 'Do you want to send this file?',
            closeButton: false,
            text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
            actions: this.createActions([`Send`, `Cancel`])
        };
        const btnFirstProps = {
            backgroundColor: "#00ffff",
            text: "Open first modal",
            onClick: this.openFirstModal.bind(this)
        };
        const btnSecondProps = {
            backgroundColor: "#00ffff",
            text: "Open second modal",
            onClick: this.openSecondModal.bind(this)
        };

        return (
            <React.Fragment>
                <OpenModalBtn text={btnFirstProps.text}
                              onClick={btnFirstProps.onClick}
                              backgroundColor={btnFirstProps.backgroundColor}/>
                <OpenModalBtn text={btnSecondProps.text}
                              onClick={btnSecondProps.onClick}
                              backgroundColor={btnSecondProps.backgroundColor}/>
                {this.state.first && <Modal closeModal={this.closeModal.bind(this)}
                                            header={firstModal.header}
                                            closeButton={firstModal.closeButton}
                                            text={firstModal.text}
                                            actions={firstModal.actions}/>}
                {this.state.second && <Modal closeModal={this.closeModal.bind(this)}
                                             header={secondModal.header}
                                             closeButton={secondModal.closeButton}
                                             text={secondModal.text}
                                             actions={secondModal.actions}/>}
            </React.Fragment>
        )
    }
}
