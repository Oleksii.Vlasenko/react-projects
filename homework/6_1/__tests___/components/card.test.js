import {shallow} from "enzyme";
import Card from "../../src/components/Section/CardList/Card/Card";
import React from "react";

describe("some test", () => {
    it("render Card", () =>
    expect(
        shallow(<Card/>).find('h3').length
    ).toBe(1))
});
