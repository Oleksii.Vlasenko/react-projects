import {combineReducers} from "redux";
import cart from "./cart";
import favorite from "./favorite";
import modal from "./modal";
import cardList from "./cardList";
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
    cart, favorite, modal, cardList, form: formReducer
});

export default rootReducer;
