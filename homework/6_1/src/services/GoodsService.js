function GoodsService() {
    return fetch(`./data.json`)
        .then(response => response.json());
}

export default GoodsService;
