import React from "react";
import styled from 'styled-components';
import Card from "../CardList/Card/Card";
import {useSelector} from "react-redux";

const CartBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Cart = ({action}) => {

    let state = useSelector(state => state.cart);

    const cardModal = {
        header: 'Remove from cart',
        closeButton: true,
        text: `Do you want to remove this album?`,
        actionBtn: [`Remove`, `Cancel`]
    };

    return (
        <CartBlock>
            {state && (state.length > 0) && state.map((item, index) => {
                return (
                    <Card key={index}
                          album={item}
                          btnRemTitle={'Remove'}
                          cardModal={cardModal}
                          action={action}/>
                )
            })}
        </CartBlock>
    )
};

export default Cart;
