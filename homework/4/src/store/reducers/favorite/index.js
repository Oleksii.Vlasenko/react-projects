import C from "../../consts";

const initialState = [];

const addToFavorite = (state, action) => {
    return [
        ...state,
        action.album
    ]
};

const removeFromFavorite = (state, action) => {
    return state.filter((item) => item.article !== action.album.article)
};

const getLocalRequest = (item) => {
    const state = JSON.parse(localStorage.getItem(`favorite`)) || [];
    return state;
};

const favorite = (state = initialState, action) => {
    switch (action.type) {
        case C.ADD_TO_FAVORITE:
            return addToFavorite(state, action);
        case C.REM_FROM_FAVORITE:
            return removeFromFavorite(state, action);
        case C.GET_REQUEST:
            return getLocalRequest();
        default:
            return state;

    }
};

export default favorite;
