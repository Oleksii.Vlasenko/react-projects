import {combineReducers} from "redux";
import cart from "./cart";
import favorite from "./favorite";
import modal from "./modal";
import cardList from "./cardList";

const rootReducer = combineReducers({
    cart, favorite, modal, cardList
});

export default rootReducer;
