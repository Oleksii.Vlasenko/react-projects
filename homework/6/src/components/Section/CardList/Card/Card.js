import React, {useState, useEffect} from "react";
import styled from 'styled-components'
import {useDispatch, useSelector} from "react-redux";
import {
    addToFavorite,
    removeFromFavorite
} from "../../../../store/actions";

const CardItem = styled.div`
   position: relative;
   width: 200px;
   height: 300px;
   padding: 5px;
   background-color: #191919;
   background-size: contain;
   background-position: 50% 60%;
   background-repeat: no-repeat;
   text-align: center;
   margin: 40px;
   color: #ffffff;
   border-radius: 5px;
`;

const BandName = styled.h3`
    margin: 0;
    background-color: #ff5500;
`;

const AlbumName = styled.h4`
    margin: 0;
    background-color: #ff5500;
`;

const Price = styled.div`
    position: absolute;
    bottom: 10px;
    left: 10px;
    font-size: 18px;
    font-weight: 700;
    border-radius: 5px;
`;

const AddToCart = styled.button`
    position: absolute;
    bottom: -15px;
    right: -15px;
    background-color: #ff3300;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
`;

const AddToFavorite = styled.button`
    width: 30px;
    height: 30px;
    position: absolute;
    top: 70px;
    left: -15px;
    background-color: #ff330088;
    background-image: url("/img/notFavorite.png");
    background-size: contain;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
`;

function Card(props) {
    let {album, cardModal, btnRemTitle, action} = props;

    const isFavorite = () => state.favorite.find((item) => item.article === album.article) && true;

    const state = useSelector(state => state);
    const dispatch = useDispatch();

    let [cartBtn, setCartBtn] = useState('Add to cart');

    useEffect(() => {
        state.cart.find(item => item.article === album.article)
            ? setCartBtn(btnRemTitle)
            : setCartBtn('Add to cart');
    }, [state.cart, btnRemTitle, album.article]);

    const initCartModal = () => {
        if (cartBtn !== 'In cart') {
            action(cardModal, album);
        }
    };

    const initFavorite = () => {
        if (isFavorite()) {
            dispatch(removeFromFavorite(album));
        } else {
            dispatch(addToFavorite(album));
        }
    };

    return (
        <React.Fragment>
            <CardItem style={{
                backgroundImage: `url(${album.front})`,
                borderTop: `5px solid ${album.color}`,
                borderBottom: `5px solid ${album.color}`
            }}>
                <BandName>{album.title.split("-")[0]}</BandName>
                <AlbumName>{album.title.split("-")[1]}</AlbumName>
                <Price>{album.price} $</Price>
                <AddToCart onClick={initCartModal}>{cartBtn}</AddToCart>
                <AddToFavorite onClick={initFavorite}
                               style={{
                                   backgroundImage: `url(${
                                       isFavorite()
                                           ? '/img/favorite.png'
                                           : '/img/notFavorite.png'})`
                               }}
                />
            </CardItem>
        </React.Fragment>
    )
}

export default Card;
