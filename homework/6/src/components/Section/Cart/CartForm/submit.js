import {SubmissionError} from 'redux-form'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

function submit(values) {
    return sleep(1000).then(() => {
        if (!values.firstname || values.firstname.length < 2 || values.firstname.length > 20) {
            throw new SubmissionError({
                firstname: 'Firstname error',
                _error: 'Firstname error'
            })
        } else if (!values.lastname || values.lastname.length > 20 || values.lastname.length < 2) {
            throw new SubmissionError({
                lastname: 'Lastname error',
                _error: 'Lastname error!'
            })
        } else if (!values.tel || values.tel.length < 7 || values.tel.length > 13) {
            throw new SubmissionError({
                lastname: 'Tel error',
                _error: 'Tel error!'
            })
        } else if (!values.age || isNaN(Number(values.age))) {
            throw new SubmissionError({
                age: 'Age is NaN',
                _error: 'Age error!'
            })
        } else if(!values.address) {
            throw new SubmissionError({
                address: 'Address is empty',
                _error: 'Address error!'
            })
        } else {
            return true;
        }
    })
}

export default submit;
