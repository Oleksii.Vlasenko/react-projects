import React from "react";
import styled from 'styled-components';
import Card from "../CardList/Card/Card";
import {useSelector} from "react-redux";

const FavoriteBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Favorite = ({action}) => {
    let state = useSelector(state => state);

    const cardModal = {
        header: 'Add to cart',
        closeButton: true,
        text: `Do you want to add this album?`,
        actionBtn: [`Add`, `Cancel`]
    };

    return (
        <FavoriteBlock>
            {state.favorite && (state.favorite.length > 0) && state.favorite.map((item, index) => {
                return (
                    <Card key={index}
                          album={item}
                          btnRemTitle={'In cart'}
                          cardModal={cardModal}
                          action={action}/>
                )
            })}
        </FavoriteBlock>
    )
};

export default Favorite;
