import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import customMiddleware from './middlewares/counter'
import {createLogger} from 'redux-logger'

const configureStore = (initState = {}) => {
    const logger = createLogger();
    let store = createStore(
        rootReducer,
        initState,
        applyMiddleware(customMiddleware, logger, thunk));
    return store;
};

export default configureStore;
