import {shallow, configure} from 'enzyme';
import toJson from "enzyme-to-json";
import {compose} from "redux";
import Card from "../../src/components/Section/CardList/Card/Card";
import configureStore from "../../src/store";
import {Provider} from "react-redux";
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import React from "react";
describe("<Card /> UI Component", () => {
    const shallowExpect = compose(expect, toJson, shallow);
    const album = {
        title: "The Wildhearts - Renaissance Men",
        price: "12.07",
        front: "/img/albums/front_1.jpg",
        back: "/img/albums/back_1.jpg",
        article: "0001",
        color: "#0000f0"
    };
    const btnRemTitle = 'In cart';
    let action = jest.fn();
    let store;
    beforeAll(() => {
        store = configureStore();
    });
    it('Renders correct properties', () => {
        shallowExpect(
            <Provider store={store}>
                <Card album={album}
                      btnRemTitle={btnRemTitle}
                      action={action}/>
            </Provider>
        ).toMatchSnapshot();
    });
});
