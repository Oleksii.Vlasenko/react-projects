import {shallow, configure} from 'enzyme';
import toJson from "enzyme-to-json";
import {compose} from "redux";
import Modal from "../../src/components/Section/Modal/Modal";
import configureStore from "../../src/store";
import {Provider} from "react-redux";
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import React from "react";

describe("<Modal /> UI Component", () => {
    const shallowExpect = compose(expect, toJson, shallow);

    const action = jest.fn();
    const header = 'Add to cart';
    const closeButton = true;
    const text = `Do you want to add this album?`;
    const actionBtn = [`Add`, `Cancel`];
    const createActions = (array) => {
        let key = 0;
        let result = array.map((item) => {
            return (<button key={key++}>{item}</button>)
        });
        return result;
    };

    it('Renders correct properties', () => {
        shallowExpect(
            <Modal action={action}
                   header={header}
                   closeButton={closeButton}
                   text={text}
                   actions={createActions(actionBtn)}
            />
        ).toMatchSnapshot();
    });
});
