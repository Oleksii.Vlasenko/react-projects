import React from "react";
import {mount, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Card from "../../src/components/Section/CardList/Card/Card";
import configureStore from "../../src/store";
import {Provider} from "react-redux";

configure({adapter: new Adapter()});
const album = {
    title: "The Wildhearts - Renaissance Men",
    price: "12.07",
    front: "/img/albums/front_1.jpg",
    back: "/img/albums/back_1.jpg",
    article: "0001",
    color: "#0000f0"
};

describe("creating Card", () => {

    let cardBlock;
    const btnRemTitle = 'In cart';
    let action = jest.fn();
    let store;

    beforeAll(() => {
        store = configureStore();
        cardBlock = mount(
            <Provider store={store}>
                <Card album={album} btnRemTitle={btnRemTitle} action={action}/>
            </Provider>
        );
    });
    it('should create Cart with h3', () => {
        expect(cardBlock.find('h3').length).toEqual(1);
    });
    it('should create Cart with props.album', () => {
        expect(cardBlock.find(Card).props().album).toEqual(album);
    });
    it('invokes onClick on AddToCart', () => {
        cardBlock.find('button').at(0).simulate('click');
        expect(action).toBeCalled();
    });
    it('invokes onClick on AddToFavorite', () => {
        cardBlock.find('button').at(1).simulate('click');
        expect(action).toBeCalled();
    });
});
