import React from "react";
import {mount, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Modal from "../../src/components/Section/Modal/Modal";
import configureStore from "../../src/store";
import {Provider} from "react-redux";
import modal from "../../src/store/reducers/modal";

configure({adapter: new Adapter()});

describe("creating Card", () => {

    let modalBlock;
    const action = jest.fn();
    const header = 'Add to cart';
    const closeButton = true;
    const text = `Do you want to add this album?`;
    const actionBtn = [`Add`, `Cancel`];
    const createActions = (array) => {
        let key = 0;
        let result = array.map((item) => {
            return (<button key={key++} onClick={action}>{item}</button>)
        });
        return result;
    };

    beforeAll(() => {
        modalBlock = mount(
            <Modal action={action}
                   header={header}
                   closeButton={closeButton}
                   text={text}
                   actions={createActions(actionBtn)}/>
        );
    });
    it('should create Modal with divs', () => {
        expect(modalBlock.find('div').length).toEqual(4);
    });
    it('should create Cart with props.album', () => {
        expect(modalBlock.find('button').length).toEqual(3);
    });
    it('invokes onClick on 1st btn', () => {
        modalBlock.find('button').at(0).simulate('click');
        expect(action).toBeCalled();
    });
    it('invokes onClick on 2nd btn', () => {
        modalBlock.find('button').at(1).simulate('click');
        expect(action).toBeCalled();
    });
    it('invokes onClick on 3rd btn', () => {
        modalBlock.find('button').at(2).simulate('click');
        expect(action).toBeCalled();
    });
});
