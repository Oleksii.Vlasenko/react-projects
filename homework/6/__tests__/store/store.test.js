import configureStore from "../../src/store";
import C from "../../src/store/consts";

describe("add store success", () => {
    const album = {
        title: "The Wildhearts - Renaissance Men",
        price: "12.07",
        front: "/img/albums/front_1.jpg",
        back: "/img/albums/back_1.jpg",
        article: "0001",
        color: "#0000f0"
    };
    const action = {
        type: C.ADD_TO_FAVORITE,
        album
    };
    let store;
    beforeAll(() => {
        store = configureStore();
        store.dispatch(action);
    });
    it('should toBeDefined article', () => {
        expect(store.getState().favorite[0].article).toBeDefined();
    });
    it('should toBe price', () => {
        expect(store.getState().favorite[0].price).toBe('12.07');
    });
});
