import C from "../../../src/store/consts/index";
import cardList from "../../../src/store/reducers/cardList";

describe("get request", () => {
    it("GET_REQUEST success", () => {
        const state = [];
        const action = {
            type: C.GET_REQUEST,
            store: {test: 'test'}
        };
        const results = cardList(state, action);
        expect(results)
            .toEqual({test: 'test'});
    })
});
