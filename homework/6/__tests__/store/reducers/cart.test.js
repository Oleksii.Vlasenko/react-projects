import C from "../../../src/store/consts/index";
import cart from "../../../src/store/reducers/cart";

describe("get request", () => {
    it("ADD_TO_CART success", () => {
        const state = [];
        const action = {
            type: C.ADD_TO_CART,
            album: {
                test: 'test'
            }
        };
        const results = cart(state, action);
        expect(results)
            .toEqual([{test: 'test'}]);
    });
    it("REM_FROM_CART success", () => {
        const state = [{article: "0001"}, {article: "0002"}];
        const action = {
            type: C.REM_FROM_CART,
            album: {
                article: "0001"
            }
        };
        const results = cart(state, action);
        expect(results)
            .toEqual([{article: "0002"}]);
    })
});
