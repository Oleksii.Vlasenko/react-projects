import React, {Component} from "react";
import styled from 'styled-components';
import Modal from "./Modal";

const CardItem = styled.div`
   position: relative;
   width: 200px;
   height: 300px;
   padding: 5px;
   background-color: #191919;
   background-size: contain;
   background-position: 50% 60%;
   background-repeat: no-repeat;
   text-align: center;
   margin: 40px;
   color: #ffffff;
   border-radius: 5px;
`;

const BandName = styled.h3`
    margin: 0;
    background-color: #ff5500;
`;

const AlbumName = styled.h4`
    margin: 0;
    background-color: #ff5500;
`;

const Price = styled.div`
    position: absolute;
    bottom: 10px;
    left: 10px;
    font-size: 18px;
    font-weight: 700;
    border-radius: 5px;
`;

const AddToCart = styled.button`
    position: absolute;
    bottom: -15px;
    right: -15px;
    background-color: #ff3300;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
`;

const AddToFavorite = styled.button`
    width: 30px;
    height: 30px;
    position: absolute;
    top: 70px;
    left: -15px;
    background-color: #ff330088;
    background-image: url("/img/notFavorite.png");
    background-size: contain;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
    
`;

const ConfirmBtn = styled.button`
    width: 101px;
    height: 41px;
    border-radius: 3px;
    background-color: #b3382c;
    color: #ffffff;
    font-size: 15px;
    border: none;
    margin: 0 5px;
`;

export default class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favorite: this.isFavorite(),
            cartModal: false
        };
    }

    initCartModal() {
        this.setState(() => ({cartModal: true}))
    }

    initCart(value) {
        if (value) {
            let carts;
            localStorage.getItem(`cart`) ? carts = JSON.parse(localStorage.getItem(`cart`)) : carts = [];
            carts.push(this.props.album.article);
            localStorage.setItem(`cart`, JSON.stringify(carts));
        }
    }

    closeModal(event) {
        this.setState(() => ({cartModal: false}));
        if (event === `Add`) {
            this.initCart(true);
        } else {
            this.initCart(false);
        }
    }

    initFavorite() {
        let articles = JSON.parse(localStorage.getItem(`favorite`));
        if (this.state.favorite) {
            this.setState(() => ({favorite: false}));
            articles.splice(articles.indexOf(this.props.album.article), 1);
            localStorage.setItem(`favorite`, JSON.stringify(articles));
        } else {
            this.setState(() => ({favorite: true}));
            articles.push(this.props.album.article);
            localStorage.setItem(`favorite`, JSON.stringify(articles));
        }
    }

    isFavorite() {
        let articles = JSON.parse(localStorage.getItem(`favorite`));
        return articles.includes(this.props.album.article);
    }

    createActions(array) {
        let key = 0;
        let result = array.map((item) => <ConfirmBtn onClick={this.closeModal.bind(this, item)}
                                                     key={key++}>{item}</ConfirmBtn>);
        return result;
    }

    render() {
        const cart = {
            header: 'Do you want to add this album?',
            closeButton: true,
            text: `Do you want to add ${this.props.album.title} (price - ${this.props.album.price}, article - ${this.props.album.article})?`,
            actions: this.createActions([`Add`, `Cancel`])
        };
        return (
            <React.Fragment>
                <CardItem style={{
                    backgroundImage: `url(${this.props.album.front})`,
                    borderTop: `5px solid ${this.props.album.color}`,
                    borderBottom: `5px solid ${this.props.album.color}`
                }}>
                    <BandName>{this.props.album.title.split("-")[0]}</BandName>
                    <AlbumName>{this.props.album.title.split("-")[1]}</AlbumName>
                    <Price>{this.props.album.price} $</Price>
                    <AddToCart onClick={this.initCartModal.bind(this)}>Add to cart</AddToCart>
                    <AddToFavorite onClick={this.initFavorite.bind(this)}
                                   style={{
                                       backgroundImage: `url(${
                                           this.state.favorite
                                               ? '/img/favorite.png'
                                               : '/img/notFavorite.png'})`
                                   }}
                    />
                </CardItem>
                {this.state.cartModal && <Modal closeModal={this.closeModal.bind(this)}
                                                header={cart.header}
                                                closeButton={cart.closeButton}
                                                text={cart.text}
                                                actions={cart.actions}/>}
            </React.Fragment>
        )
    }
}
