import React, {Component} from "react";
import styled from 'styled-components';
import Card from "./Card";

const GoodsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export default class CardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartModal: false,
            title: ``,
            price: 0,
            article: 0
        }
    }

    render() {
        return (
            <GoodsContainer>
                {this.props.albums && this.props.albums.map((item, index) => <Card key={index}
                                                                                   album={item}
                                                                                   />)}
            </GoodsContainer>
        )
    }
}
