import React, {Component} from "react";
import CardList from "./CardList";
import styled from 'styled-components';

const SectionContainer = styled.div`
    background-image: url(/img/bg_section.jpg);
    background-size: cover;
`;

export default class Section extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SectionContainer>
                <CardList albums={this.props.albums}></CardList>
            </SectionContainer>
        )
    }
}
