import React, {Component} from 'react';
import styled from 'styled-components';
import Section from '../components/Section/index'
import GoodsService from "../services";

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
    }

    async componentDidMount() {
        let list = await GoodsService.getAlbums();
        this.setState({list});
    }

    render() {
        let goodsList = this.state.list;
        return (
            <React.Fragment>
                <Section albums={goodsList}></Section>
            </React.Fragment>
        )
    }
}
