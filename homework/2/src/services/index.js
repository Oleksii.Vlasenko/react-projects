export default class GoodsService {
    static async getAlbums() {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(`GET`, `./data.json`, false);
            xhr.onload = () => {
                if (xhr.status >= 400) {
                    reject(xhr.response);
                } else {
                    resolve(JSON.parse(xhr.response));
                }
            };
            xhr.onerror = () => {
                reject(xhr.response)
            };
            xhr.send();
        });

        // return fetch(`./data.json`)
        //     .then(response => response.json());
    }
}
