import React from "react";
import styled from 'styled-components'
import {Link} from "react-router-dom";

const HeaderBlock = styled.header`
  width: 100%;
  height: 100px;
  background-color: #f0f0f0;
`;

const List = styled.ul`
   margin: 0;
`;

const ListItem = styled.li`
    display: inline-block;
    margin: 0 20px;
`;

const Header = () => (
    <HeaderBlock>
            <List>
                <ListItem><Link to='/'>CARDLIST</Link></ListItem>
                <ListItem><Link to='/cart'>CART</Link></ListItem>
                <ListItem><Link to='/favorite'>FAVORITE</Link></ListItem>
            </List>
    </HeaderBlock>
);

export default Header;
