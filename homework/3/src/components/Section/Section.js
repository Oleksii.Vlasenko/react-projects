import React from "react";
import styled from 'styled-components'
import CardList from "./CardList/CardList";
import Cart from "./Cart";
import Favorite from "./Favorite";
import {Switch, Route} from "react-router-dom";

const SectionContainer = styled.div`
    background-image: url(/img/bg_section.jpg);
    background-size: cover;
`;

const Section = ({albums}) => {
     return (
         <SectionContainer>
             <Switch>
                 <Route exact path='/' render={() => <CardList albums={albums}/>}/>
                 <Route path='/cart' render={() => <Cart albums={albums}/>}/>
                 <Route path='/favorite' render={() => <Favorite albums={albums}/>}/>
             </Switch>
         </SectionContainer>
    )
};

export default Section;
