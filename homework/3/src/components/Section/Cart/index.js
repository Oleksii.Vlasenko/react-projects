import React, {useEffect, useState} from "react";
import styled from 'styled-components';
import Card from "../CardList/Card/Card";

const CartBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Cart = ({albums}) => {

    const cardModal = {
        header: 'Remove from cart',
        closeButton: true,
        text: `Do you want to remove this album?`,
        actionBtn: [`Remove`, `Cancel`]
    };

    let [carts, setCarts] = useState([]);
    useEffect(() => setCarts(JSON.parse(localStorage.getItem(`cart`))), []);

    const removeCart = () => {
        setCarts(JSON.parse(localStorage.getItem(`cart`)));
    };

    return (
        <CartBlock>
            {albums && carts && albums.map((item, index) => {
                if (carts.includes(item.article)) return (
                    <Card key={index}
                          album={item}
                          action={removeCart}
                          // btnRemTitle={'X'}
                          btnRemTitle={'Remove'}
                          cardModal={cardModal}/>
                )
            })}
        </CartBlock>
    )
};

export default Cart;
