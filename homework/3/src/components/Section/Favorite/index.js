import React, {useEffect, useState} from "react";
import styled from 'styled-components';
import Card from "../CardList/Card/Card";

const FavoriteBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Favorite = ({albums}) => {

    const cardModal = {
        header: 'Add to cart',
        closeButton: true,
        text: `Do you want to add this album?`,
        actionBtn: [`Add`, `Cancel`]
    };

    let [favorites, setFavorites] = useState([]);
    useEffect(() => setFavorites(JSON.parse(localStorage.getItem(`favorite`))),[]);

    const removeFavorite = () => {
        setFavorites(JSON.parse(localStorage.getItem(`favorite`)));
    };

    return (
        <FavoriteBlock>
            {albums && favorites && albums.map((item, index) => {
                if (favorites.includes(item.article)) return (
                    <Card key={index}
                          album={item}
                          action={removeFavorite}
                          btnRemTitle={'In cart'}
                          cardModal={cardModal}/>
                )
            })}
        </FavoriteBlock>
    )
};

export default Favorite;
