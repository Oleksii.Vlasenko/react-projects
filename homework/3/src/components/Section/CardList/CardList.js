import React from "react";
import styled from 'styled-components'
import Card from "./Card/Card";

const GoodsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

function CardList({albums}) {

    const cardModal = {
            header: 'Add to cart',
            closeButton: true,
            text: `Do you want to add this album?`,
            actionBtn: [`Add`, `Cancel`]
        };

    return (
        <GoodsContainer>
            {albums && albums.map((item, index) => <Card key={index}
                                                         album={item}
                                                         cardModal={cardModal}
                                                         btnRemTitle={'In cart'}/>)}
        </GoodsContainer>
    )
}

export default CardList;
