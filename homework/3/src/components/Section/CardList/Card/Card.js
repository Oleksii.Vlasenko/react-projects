import React, {useState, useEffect} from "react";
import styled from 'styled-components'
import Modal from "./Modal/Modal";

const ConfirmBtn = styled.button`
    width: 101px;
    height: 41px;
    border-radius: 3px;
    background-color: #b3382c;
    color: #ffffff;
    font-size: 15px;
    border: none;
    margin: 0 5px;
`;

const CardItem = styled.div`
   position: relative;
   width: 200px;
   height: 300px;
   padding: 5px;
   background-color: #191919;
   background-size: contain;
   background-position: 50% 60%;
   background-repeat: no-repeat;
   text-align: center;
   margin: 40px;
   color: #ffffff;
   border-radius: 5px;
`;

const BandName = styled.h3`
    margin: 0;
    background-color: #ff5500;
`;

const AlbumName = styled.h4`
    margin: 0;
    background-color: #ff5500;
`;

const Price = styled.div`
    position: absolute;
    bottom: 10px;
    left: 10px;
    font-size: 18px;
    font-weight: 700;
    border-radius: 5px;
`;

const AddToCart = styled.button`
    position: absolute;
    bottom: -15px;
    right: -15px;
    background-color: #ff3300;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
`;

const AddToFavorite = styled.button`
    width: 30px;
    height: 30px;
    position: absolute;
    top: 70px;
    left: -15px;
    background-color: #ff330088;
    background-image: url("/img/notFavorite.png");
    background-size: contain;
    border: none;
    padding: 5px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 16px;
`;

function Card(props) {
    let {album, action, cardModal, btnRemTitle} = props;

    const isFavorite = () => {
        let articles = JSON.parse(localStorage.getItem(`favorite`));
        return articles && articles.includes(album.article);
    };

    let [favorite, setFavorite] = useState(isFavorite());
    let [cartModal, setCartModal] = useState(false);
    let [cartBtn, setCartBtn] = useState('Add to cart');

    useEffect(() => {
        let carts = JSON.parse(localStorage.getItem('cart'));
        if (carts) {
            if (carts.includes(album.article)) {
                setCartBtn(btnRemTitle);
            } else {
                setCartBtn('Add to cart');
            }
        }
    }, []);

    const addCart = () => {
        let carts;
        localStorage.getItem(`cart`)
            ? carts = JSON.parse(localStorage.getItem(`cart`))
            : carts = [];
        if (!carts.includes(album.article)) {
            carts.push(album.article);
            localStorage.setItem(`cart`, JSON.stringify(carts));
            setCartBtn('In cart');
        }
    };

    const removeCart = () => {
        let carts = JSON.parse(localStorage.getItem(`cart`));
        carts.splice(carts.indexOf(album.article), 1);
        localStorage.setItem(`cart`, JSON.stringify(carts));
    };

    const initCartModal = () => {
        if (cartBtn !== 'In cart') {
            setCartModal(true);
        }
    };

    const closeModal = (event) => {
        setCartModal(false);
        if (event === `Add`) {
            addCart();
        }
        if (props.action && event === `Remove`) {
            removeCart();
            props.action();
        }
    };

    const initFavorite = () => {
        if (favorite) {
            setFavorite(false);
            removeFavorite();
            if (action) {
                setTimeout(() => action(), 500);
            }
        } else {
            setFavorite(true);
            addFavorite();
        }
    };

    const addFavorite = () => {
        let articles = JSON.parse(localStorage.getItem(`favorite`)) || [];
        articles.push(album.article);
        localStorage.setItem(`favorite`, JSON.stringify(articles));
    };

    const removeFavorite = () => {
        let articles = JSON.parse(localStorage.getItem(`favorite`)) || [];
        articles.splice(articles.indexOf(album.article), 1);
        localStorage.setItem(`favorite`, JSON.stringify(articles));
    };

    const createActions = (array) => {
        let key = 0;
        let result = array.map((item) => <ConfirmBtn onClick={() => closeModal(item)}
                                                     key={key++}>{item}</ConfirmBtn>);
        return result;
    };

    return (
        <React.Fragment>
            <CardItem style={{
                backgroundImage: `url(${album.front})`,
                borderTop: `5px solid ${album.color}`,
                borderBottom: `5px solid ${album.color}`
            }}>
                <BandName>{album.title.split("-")[0]}</BandName>
                <AlbumName>{album.title.split("-")[1]}</AlbumName>
                <Price>{album.price} $</Price>
                <AddToCart onClick={initCartModal}>{cartBtn}</AddToCart>
                <AddToFavorite onClick={initFavorite}
                               style={{
                                   backgroundImage: `url(${
                                       favorite
                                           ? '/img/favorite.png'
                                           : '/img/notFavorite.png'})`
                               }}
                />
            </CardItem>
            {cartModal && <Modal action={closeModal}
                                 header={cardModal.header}
                                 closeButton={cardModal.closeButton}
                                 text={cardModal.text}
                                 actions={createActions(cardModal.actionBtn)}/>}
        </React.Fragment>
    )
}

export default Card;
