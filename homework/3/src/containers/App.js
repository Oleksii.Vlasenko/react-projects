import React, {useState, useEffect} from 'react';
import Section from '../components/Section/Section';
import styled from 'styled-components';
import GoodsService from "../services/GoodsService";
import Header from "../components/Header/Header";

const Footer = styled.footer`
  width: 100%;
  height: 100px;
  background-color: #0f0f0f;
`;

function App() {
    let [list, setList] = useState([]);

    useEffect(() => {
        GoodsService()
            .then(result => setList(result));
    }, []);

    return (
        <React.Fragment>
            <Header/>
            <Section albums={list}/>
            <Footer/>
        </React.Fragment>
    );
}

export default App;
