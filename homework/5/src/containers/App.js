import React from 'react';
import Section from '../components/Section/Section';
import styled from 'styled-components';
import Header from "../components/Header/Header";
import {BrowserRouter} from "react-router-dom";
import configureStore from "../store";
import {Provider} from "react-redux";

const Footer = styled.footer`
  width: 100%;
  height: 100px;
  background-color: #0f0f0f;
`;

const store = configureStore();

store.subscribe(() => localStorage.setItem('cart', `${JSON.stringify(store.getState().cart)}`));
store.subscribe(() => localStorage.setItem('favorite', `${JSON.stringify(store.getState().favorite)}`));

export default function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header/>
                <Section/>
                <Footer/>
            </BrowserRouter>
        </Provider>
    );
}
