import C from '../consts/index'

export const addToCard = (album) => ({
    type: C.ADD_TO_CART,
    album
});

export const removeFromCart = (album) => ({
    type: C.REM_FROM_CART,
    album
});

export const addToFavorite = (album) => ({
    type: C.ADD_TO_FAVORITE,
    album
});

export const removeFromFavorite = (album) => ({
    type: C.REM_FROM_FAVORITE,
    album
});

export const modalWindow = () => ({
    type: C.MODAL,
});
