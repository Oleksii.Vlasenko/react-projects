import GoodsService from "../../services/GoodsService";
import C from "../consts";

const getAlbums = () => {
    return async function (dispatch) {
        await GoodsService()
            .then(result => {
                console.log(result)
                dispatch({
                    type: C.GET_REQUEST,
                    store: result
                })
            });
    }
};

export default getAlbums;
