import C from "../../consts";

const initialState = [];

const cardList = (state = initialState, action) => {
    switch (action.type) {
        case C.GET_REQUEST:
            return action.store;
        default:
            return state;
    }
};

export default cardList;
