import React from "react";
import styled from 'styled-components'

const ModalBackground = styled.div`
    width: 100vw;
    height: 100vh;
    background-color: #00000088;
    position: fixed;
    top: 0;
    left: 0;
    Z-index: 9999;
`;

const ModalDiv = styled.div`
    width: 520px;
    height: 250px;
    margin: 300px auto;
`;

const Header = styled.div`
    width: 100%;
    height: 68px;
    background-color: #d44637;
    line-height: 60px;
    font-size: 22px;
    color: #ffffff;
    border-radius: 5px 5px 0 0;
`;

const TextHeader = styled.span`
    margin-left: 30px;  
    font-weight: 600;
`;

const CloseBtn = styled.button`
    color: #ffffff;
    background-color: transparent;
    border: none;
    font-size: 30px;
    float: right;
    margin: 10px;
`;

const Section = styled.section`
    width: 100%;
    height: calc(100% - 69px);
    background-color: #e74c3c;
    color: #ffffff;
    padding-top: 1px;
    border-radius: 0 0 5px 5px;
`;

const SectionText = styled.p`
    margin-top: 35px;
    padding: 0 40px;
    text-align: center;
    font-size: 15px;
    line-height: 25px;
`;

const ConfirmBlock = styled.div`
    width: 100%;
    text-align: center;
    margin-top: 25px;
`;

function Modal(props) {

    let {header, text, closeButton, actions} = props;

    function closeModals(event) {
        if (event.target.classList.contains("close-modal")) {
            props.action(event);
        }
    }

    return (
        <ModalBackground className="close-modal"
                         onClick={(event) => closeModals(event)}>
            <ModalDiv>
                <Header>
                    <TextHeader>{header}</TextHeader>
                    {closeButton &&
                    <CloseBtn className="close-modal"
                              onClick={(event) => closeModals(event)}>x</CloseBtn>}
                </Header>
                <Section>
                    <SectionText>{text}</SectionText>
                    <ConfirmBlock>
                        {actions}
                    </ConfirmBlock>
                </Section>
            </ModalDiv>
        </ModalBackground>
    )
}

export default Modal;
