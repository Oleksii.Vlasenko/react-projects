import React from "react";
import styled from 'styled-components'
import Card from "./Card/Card";
import {useSelector} from "react-redux";

const GoodsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

function CardList({action}) {
    let state = useSelector(state => state);

    const cardModal = {
        header: 'Add to cart',
        closeButton: true,
        text: `Do you want to add this album?`,
        actionBtn: [`Add`, `Cancel`]
    };

    return (
        <GoodsContainer>
            {state.cardList && (state.cardList.length > 0) && state.cardList.map((item, index) => <Card key={index}
                                                                         album={item}
                                                                         btnRemTitle={'In cart'}
                                                                         cardModal={cardModal}
                                                                         action={action}/>)}
        </GoodsContainer>
    )
}

export default CardList;
